﻿# Themes for Hubzilla

[Hubzilla](http://hubzilla.org/) provides everything a community needs for an online hub (events/forums/webpages/wiki/chat/social networking) with strong privacy controls, and allows your social network to span across different social websites and not depend on or be tied down to any single website. It can federate with Gnu Social, Diaspora etc. (and even some commercial sites), but Hubzilla is a better direction to head in.

I'm maintaining a customised theme on my hub, so may as well add it to the commons:

* Provides colourblind-friend variant
* **Warning**: This theme duplicates several sections of the redbasic theme, so it should be updated if the redbasic theme is updated.


![Screenshot](homesocial/img/screenshot.png "Screenshot") 

## Installation

I suggest using Hubzilla's [add_theme_repo](https://github.com/redmatrix/hubzilla/wiki/Administrator-Guide#theme-management) command to install this theme-pack

1. Navigate to your Hubzilla web root (this should contain a `util/` directory)
  
  ```
  cd /var/www 
  ```
  
2. Add the theme repo and give it a name
  
  ```
  util/add_theme_repo https://gitlab.com/Treer/hubzilla-themes.git Treer
  ```
  
## Tips
  
* You can later update from the repo by using

  ```
  util/update_theme_repo Treer
  ```

  
* If you want to use one of the default profile icons provided by the theme, symbolic-link it into the `images/default_profile_photos/` directory then use `util/config` to set `default_profile_photo`

  ```
  pushd images/default_profile_photos
  ```
  ```
  ln -s ../../extend/theme/Treer/homesocial/assets/default_profile_photos/flat_abstract_4_green flat_abstract_4_green
  ```
  ```
  popd
  ```
  ```
  util/config system default_profile_photo flat_abstract_4_green
  ```
  
  The following profile icons are provided:
  
  ![flat abstract 1](homesocial/assets/default_profile_photos/flat_abstract_1/80.png "flat abstract 1") 
  ![flat abstract 2](homesocial/assets/default_profile_photos/flat_abstract_2/80.png "flat abstract 2")
  ![flat abstract 3](homesocial/assets/default_profile_photos/flat_abstract_3/80.png "flat abstract 3")
  ![flat abstract 4](homesocial/assets/default_profile_photos/flat_abstract_4/80.png "flat abstract 4")
  ![flat abstract 4 green](homesocial/assets/default_profile_photos/flat_abstract_4_green/80.png "flat abstract 4 green")

* The title and logo seen in the screenshot was made with the following html in the "Banner/Logo" field of the *Admin -> Site* settings:
```
<a href="https://social.domain.net"><span style="color: #769a1e; font-family: arial, helvetica, sans-serif; font-size: 210%; font-weight: normal"><img src="view/theme/homesocial/img/network_logo.png">social.domain.<span style="font-size: 50%">net</span></span></a>
```
